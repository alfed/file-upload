from django.http import JsonResponse


class Response(object):

    @staticmethod
    def success(data=[], code=200, msg="ok"):
        return JsonResponse({
            "status": True,
            "code": code,
            "msg": msg,
            "data": data
        })

    @staticmethod
    def failure(data=[], code=500, msg="error"):
        return JsonResponse({
            "status": False,
            "code": code,
            "msg": msg,
            "data": data
        })
