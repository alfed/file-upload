import json

from Utility.log import Log
from Utility.response import Response


class Access(object):
    MANAGER = False  # teacher character
    SUPERUSER = True  # root character

    @classmethod
    def check_request_body(cls, check_for_post=True, methods='POST'):
        if methods == 'GET':
            check_for_post = False

        def outer_decorator(func):
            def inner_decorator(request):

                if methods == request.method:  # compare it's request method with expected values
                    if check_for_post:  # Check request body for POST methods
                        values = None
                        try:
                            values = json.loads(request.body.decode('utf-8'))
                        except Exception as e:
                            Log.error(str(e))
                            return Response.failure(msg='Error occurred when parsing request body',
                                                    code=500)

                        if values:
                            return func(request)
                        else:
                            return Response.failure(msg='Request body is empty')

                    else:  # Query for GET request's params
                        values = request.GET
                        if values:
                            return func(request)
                        else:
                            return Response.failure(msg='Request body is empty')

                else:
                    return Response.failure(msg='Method Not Allowed', code=405)

            return inner_decorator

        return outer_decorator
