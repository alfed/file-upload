from datetime import datetime


class Log(object):

    @staticmethod
    def error(message):
        formatted_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(f'\033[31m[ ERROR ]\033[0m {formatted_time} {message}')

    @staticmethod
    def info(message):
        formatted_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(f'\033[32m[ MESSAGE ]\033[0m {formatted_time} {message}')
