import json


class Request(object):
    # Parse the request body or params as class member such as request serializer
    GET_METHOD = 0
    POST_METHOD = 1

    def __init__(self, request, method=1):
        # POST method by default

        load_data = None

        if method == self.GET_METHOD:
            load_data = request.GET

        else:
            load_data = json.loads(request.body.decode('utf-8'))

        for key, value in load_data.items():
            setattr(self, key, value)
            self.__dict__[key] = value

    @property
    def convert_to_dict(self) -> dict:
        return dict(self.__dict__)
