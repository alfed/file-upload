class Store(object):
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
            self.__dict__[key] = value

    @property
    def convert(self) -> dict:
        return dict(self.__dict__)
