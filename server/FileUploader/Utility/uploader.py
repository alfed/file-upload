# life is short , you need python ~

import hashlib
from datetime import datetime
import os

from uploader.models import UploadFormModel

from Utility.log import Log


class UploadPrepare(object):
    ROOT_PATH = r'files/'
    LEARN_PATH = r'Learning/'
    PROG_PATH = r'Programming/'
    ANNUNC_PATH = r'Announce/'

    @classmethod
    def create_directory(cls, type_name, hexed_name):
        type_path = os.path.join(cls.ROOT_PATH, type_name)
        final_path = os.path.join(type_path, hexed_name)

        if not os.path.exists(type_path):
            os.makedirs(type_path)
            Log.info(f'directory {type_path} created')

        os.makedirs(final_path, exist_ok=True)
        Log.info(f'directory {final_path} created')

    @staticmethod
    def generate_directory_name(types):
        now = datetime.now()
        formatted_now = now.strftime('%Y-%m-%d %H:%M:%S')
        hex_format = f'{formatted_now}-{types}'
        return hashlib.md5(hex_format.encode()).hexdigest()


class Uploader(UploadPrepare):
    def __init__(self, index):
        file_instance = UploadFormModel.objects.filter(id=index).first()
        self.full_path = os.path.join(file_instance.post_type, file_instance.file_directory_name)

    def get_path(self):
        return os.path.join(self.ROOT_PATH, self.full_path)
