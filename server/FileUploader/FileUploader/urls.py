"""
URL configuration for FileUploader project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.urls import path
from uploader.views import JsonStorageView, UploadFormView, FileQueryView


urlpatterns = [
    # Test api
    path('test/api/json_store', JsonStorageView.view_store_json_data, name="store json"),
    path('test/api/json_query', JsonStorageView.view_query_json_data, name="query json"),
    path('test/api/json_fields_query', JsonStorageView.view_json_field_query, name="query json fields"),

    # Upload api
    path('api/formupload', UploadFormView.view_upload_form, name="upload form"),
    path('api/fileupload', UploadFormView.view_upload_file, name="upload file"),
    path('api/getpostlist', FileQueryView.view_file_query, name='file query'),
    path('api/get_file_group', FileQueryView.view_download_file_query, name='download file query'),

]
