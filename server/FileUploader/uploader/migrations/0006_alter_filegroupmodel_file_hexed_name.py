# Generated by Django 5.0.6 on 2024-08-25 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0005_uploadformmodel_file_directory_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filegroupmodel',
            name='file_hexed_name',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
