import os

from django.shortcuts import render

# life is short , you need python.

import json

# Create your views here.
from Utility.request import Request
from Utility.response import Response
from Utility.store import Store
from Utility.access import Access
from Utility.uploader import Uploader, UploadPrepare
from Utility.log import Log

from uploader.models import JsonStoreModel, UploadFormModel, FileGroupModel


class JsonStorageView(object):
    @staticmethod
    @Access.check_request_body()
    def view_store_json_data(request):
        request_payload = Request(request)
        json_model = JsonStoreModel(
            target_id=request_payload.target_id,
            form_data=request_payload.preference
        )

        json_model.save()
        return Response.success()

    @staticmethod
    @Access.check_request_body(methods='GET')
    def view_query_json_data(request):
        request_payload = Request(request, method=Request.GET_METHOD)
        json_model = JsonStoreModel.objects.filter(target_id=request_payload.target_id).first()
        if json_model:
            json_data = Store(form_data=json_model.form_data)
            return Response.success(data=json_data.convert)
        else:
            return Response.failure(msg="empty form data")

    @staticmethod
    @Access.check_request_body(methods='POST')
    def view_json_field_query(request):
        request_payload = Request(request, method=Request.GET_METHOD)
        json_model = JsonStoreModel.objects.filter(form_data__sms=request_payload.sms)
        form_array = []
        for items in json_model:
            store_item = Store(target_id=items.target_id, form_data=items.form_data)
            form_array.append(store_item.convert)

        if json_model:
            return Response.success(data=form_array)
        else:
            return Response.failure(msg="empty form data")


class UploadFormView(object):

    @staticmethod
    def _write_chunk_and_store(file, path, key):
        file_path = os.path.join(path, file.name)
        with open(file_path, 'wb') as f:
            for chunk in file.chunks():
                f.write(chunk)
                Log.info(f'File {file} has saved in {file_path}')

        # store it in db
        file_group_instance = FileGroupModel(
            file_group_id=key,
            file_real_name=file.name,
            content_type=file.content_type,
            file_size=file.size
        )

        file_group_instance.save()

    @staticmethod
    @Access.check_request_body()
    def view_upload_form(request):
        # first create user form and save it , then return the id
        # for create new file group instance , this is the key
        request_payload = Request(request)

        file_directory = None
        if request_payload.file_length > 0:
            file_directory = UploadPrepare.generate_directory_name(request_payload.types)
            UploadPrepare.create_directory(request_payload.types, file_directory)

        form_model = UploadFormModel(
            post_title=request_payload.title,
            post_type=request_payload.types,
            post_description=request_payload.contents,
            file_directory_name=file_directory,
        )

        form_model.save()

        # return its id for save as file group id
        store_instance = Store(file_group=form_model.id)
        return Response.success(data=store_instance.convert)

    @classmethod
    def view_upload_file(cls, request):
        request_payload = Request(request, Request.GET_METHOD)

        upload_file = request.FILES
        uploader_instance = Uploader(request_payload.index_num)
        path = uploader_instance.get_path()

        for file in upload_file.getlist('fileObject'):
            cls._write_chunk_and_store(file, path, request_payload.index_num)

        return Response.success()


class FileQueryView(object):
    @staticmethod
    def view_file_query(request):
        group_instance = UploadFormModel.objects.all()
        form_array = []
        for group in group_instance:
            group_store = Store(
                title=group.post_title,
                type=group.post_type,
                content=group.post_description,
                file="Jump to download",
                link=f'/files?index={group.id}'
            )
            form_array.append(group_store.convert)
        return Response.success(data=form_array)

    @staticmethod
    def view_download_file_query(request):
        index_num = request.GET.get('index')

        file_group_instance = None
        form_instance = None
        form_array = []

        if int(index_num) < 0:
            file_group_instance = FileGroupModel.objects.all()
            form_instance = UploadFormModel.objects.all()

            for form_table in form_instance:
                for file_table in file_group_instance:
                    if form_table.id == file_table.file_group_id:
                        formated_date = file_table.upload_date.now().strftime('%Y-%m-%d %H:%M:%S')
                        group_store = Store(
                            file_name=file_table.file_real_name,
                            update_date=formated_date,
                            actual_size=file_table.file_size,
                            types=file_table.content_type,
                            link=f'http://localhost/downloads/{form_table.post_type}/{form_table.file_directory_name}/{file_table.file_real_name}'
                        )

                        form_array.append(group_store.convert)
        else:
            file_group_instance = FileGroupModel.objects.filter(file_group_id=index_num)
            form_instance = UploadFormModel.objects.filter(id=index_num).first()

            for group in file_group_instance:
                formated_date = group.upload_date.now().strftime('%Y-%m-%d %H:%M:%S')

                group_store = Store(
                    file_name=group.file_real_name,
                    update_date=formated_date,
                    actual_size=group.file_size,
                    types=group.content_type,
                    link=f'http://localhost/downloads/{form_instance.post_type}/{form_instance.file_directory_name}/{group.file_real_name}'
                )
                print(group_store.convert)
                form_array.append(group_store.convert)

        return Response.success(data=form_array)
