from django.db import models
from django.db.models import JSONField

from django.utils import timezone
from datetime import datetime


# Create your models here.


class JsonStoreModel(models.Model):
    target_id = models.IntegerField(primary_key=True, default=0)
    form_data = JSONField()


class UploadFormModel(models.Model):
    # id -> file_group_id
    post_title = models.CharField(max_length=100, null=False, blank=True)
    post_type = models.CharField(max_length=30, null=False, blank=True)
    post_description = models.TextField(null=False, blank=True)
    file_directory_name = models.CharField(max_length=126, null=True, blank=True)


class FileGroupModel(models.Model):
    file_group_id = models.IntegerField(default=-1)
    file_real_name = models.CharField(max_length=200, null=False, blank=True, default='')
    file_size = models.IntegerField(null=False, blank=True, default=0)
    content_type = models.CharField(max_length=200, null=False, blank=True)
    upload_date = models.DateTimeField(auto_now_add=True)

