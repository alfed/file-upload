import { createRouter, createWebHistory } from 'vue-router'
import CreateNew from '@/views/CreateNew.vue';
import HomePage from '@/views/HomePage.vue';
import ListsView from '@/views/AllLists.vue'
import FilesView from '@/views/FIlesView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomePage
    },
    {
      path: '/create',
      name: 'Create',
      component: CreateNew
    },
    ,
    {
      path: '/list',
      name: 'List',
      component: ListsView
    },
    {
      path: '/files',
      name: 'files',
      component: FilesView
      }
  ]
})

export default router
