import './assets/base.css'

import { createApp } from 'vue'
// import * from 
import App from './App.vue'
import router from './router'

import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'

const vuetify = createVuetify({
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi,
    },
  },
    theme: {
        defaultTheme: 'light',
      },
  components,
  directives,
})


const app = createApp(App)
app.use(vuetify)
app.use(router)
app.mount('#app')
