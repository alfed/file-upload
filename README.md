# File Uploader

`sami.develop@outlook.com`  `Abdusami` 

`Django + Vue3 + PostgreSQL`

- [Introdoctions](##Introdoctions) 
- [Project Struct](##Project Struct)
- [Get started](##Get started)

## Introdoctions

This project uses **Django** as the backend server, **PostgreSQL**  as the main database, and **Vue3** as the frontend framework. The UI library used is the **Vuetifuyjs** component library, which is a completely separated file upload website for `MVC` mode.



## Project Structs



```bash
- doc       # documents
- server    # server source file
- static    # precompiled static file (builded)
- web       # front-end pages source file (vue3)
- file.conf # Nginx config file for configuration
- run       # run back-end server script
```





## Get started



### Database Configuration

there is a database called `file_upload` , we need to configure in `settings.py` database :

```bash
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'file_upload',
        'USER': ' **** ',
        'PASSWORD': ' **** ',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}
```



### Nginx Configuration

- To start the project, it is necessary to first configure the Nginx static server, Paste the following code into the main configuration file of the Nginx server

```nginx
http{

	# ....
	# ....
	
	include /path/to/project/root/directory/file.conf;

}
```



> Don't forget to point to the static folder under the root directory of the project from the root directory in the file. conf file



### Start WSGI Server

- First, install the required pip library from the requirements. txt file in the root directory

```bash
pip install -r requirements.txt
```



- Starting the WSGI server from the command line

```bash
$ nohup gunicorn -w 4 -b 0.0.0.0:2424 FileUploader.wsgi & > ./gunicorn.log
```

Successfully deployed, can access on `http://localhost`  web borwser



































